package me.bumblebee.railminer.upgrades;

import me.bumblebee.railminer.Miner;

public class FuelEfficiencyUpgrade implements Upgrade {

    Miner m;
    public FuelEfficiencyUpgrade(Miner m) {
        this.m = m;
    }

    @Override
    public void run() {
        m.fuelRequired = 1;
    }
}
