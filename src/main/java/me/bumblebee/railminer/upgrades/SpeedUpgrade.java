package me.bumblebee.railminer.upgrades;

import me.bumblebee.railminer.Miner;

public class SpeedUpgrade implements Upgrade {

    Miner m;
    public SpeedUpgrade(Miner m) {
        this.m = m;
    }

    @Override
    public void run() {
        m.speedSettingOne = 10;
        m.speedSettingTwo = 5;
    }

}
