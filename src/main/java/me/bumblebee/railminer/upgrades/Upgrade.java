package me.bumblebee.railminer.upgrades;

public interface Upgrade {

    public void run();

}
