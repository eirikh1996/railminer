package me.bumblebee.railminer.upgrades;

public interface ReturnableUpgrade {

    public Object run();

}
