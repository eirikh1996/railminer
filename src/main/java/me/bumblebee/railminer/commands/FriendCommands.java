package me.bumblebee.railminer.commands;

import me.bumblebee.railminer.RailMiner;
import me.bumblebee.railminer.managers.Messages;
import me.bumblebee.railminer.managers.UpgradeManager;
import me.bumblebee.railminer.utils.Friends;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

public class FriendCommands implements CommandExecutor {

    Friends f = new Friends();
    UpgradeManager upgrades = new UpgradeManager();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        if (cmd.getName().equals("railminer")) {
            if (!(args.length > 0)) {
                sender.sendMessage(Messages.INVALID_ARGS.get());
                return false;
            }
            if (args[0].equals("friend") || args[0].equals("f")) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Console cannot have friends :(");
                    return false;
                }
                Player p = (Player) sender;
                if (!(args.length > 1)) {
                    sender.sendMessage(Messages.INVALID_ARGS.get());
                    return false;
                }
                if (args[1].equalsIgnoreCase("help")) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6/railminer friend add <player>&f: Add <player> to your miners"));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6/railminer friend del <player>&f: Remove <player> from your miners"));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6/railminer friend list &f: Show all the players that have access to your miners"));
                } else if (args[1].equalsIgnoreCase("add")) {
                    if (!p.hasPermission("railminer.friends")) {
                        sender.sendMessage(Messages.NO_PERMISSIONS.get());
                        return false;
                    }
                    if (!(args.length > 2)) {
                        sender.sendMessage(Messages.INVALID_ARGS.get());
                        return false;
                    }

                    Player t = Bukkit.getServer().getPlayer(args[2]);
                    if (t == null) {
                        sender.sendMessage(Messages.NOT_ONLINE.get().replace("%name%", args[2]));
                        return false;
                    }

                    if (f.isFriendsWith(p.getUniqueId(), t.getUniqueId())) {
                        sender.sendMessage(Messages.ALREADY_FRIENDS.get().replace("%name%", t.getName()));
                        return false;
                    }

                    f.addFriend(p.getUniqueId(), t.getUniqueId());
                    sender.sendMessage(Messages.FRIEND_ADDED.get().replace("%name%", t.getName()));
                    return true;
                } else if (args[1].equalsIgnoreCase("del")) {
                    if (!p.hasPermission("railminer.friends")) {
                        sender.sendMessage(Messages.NO_PERMISSIONS.get());
                        return false;
                    }

                    if (!(args.length > 2)) {
                        sender.sendMessage(Messages.INVALID_ARGS.get());
                        return false;
                    }

                    Player t = Bukkit.getServer().getPlayer(args[2]);
                    if (t == null) {
                        sender.sendMessage(Messages.NOT_ONLINE.get().replace("%name%", args[2]));
                        return false;
                    }

                    if (!f.isFriendsWith(p.getUniqueId(), t.getUniqueId())) {
                        sender.sendMessage(Messages.NOT_FRIENDS.get().replace("%name%", t.getName()));
                        return false;
                    }

                    f.delFriend(p.getUniqueId(), t.getUniqueId());
                    sender.sendMessage(Messages.FRIEND_REMOVED.get().replace("%name%", t.getName()));
                    return true;
                } else if (args[1].equalsIgnoreCase("list")) {
                    if (!p.hasPermission("railminer.friends")) {
                        sender.sendMessage(Messages.NO_PERMISSIONS.get());
                        return false;
                    }

                    StringBuilder names = new StringBuilder();
                    if (!Friends.friends.containsKey(p.getUniqueId())) {
                        sender.sendMessage(Messages.NO_FRIENDS.get());
                        return false;
                    }
                    for (UUID uuid : f.getFriends(p.getUniqueId())) {
                        Player t = Bukkit.getServer().getPlayer(uuid);
                        if (t == null) {
                            OfflinePlayer ot = Bukkit.getServer().getOfflinePlayer(uuid);
                            names.append(ot.getName()).append(" ");
                        } else {
                            names.append(t.getName()).append(" ");
                        }
                    }
                    sender.sendMessage(Messages.FRIENDS_WITH.get().replace("%list%", names.toString()));
                return true;
                }
            } else if (args[0].equalsIgnoreCase("reload")) {
                if (!sender.hasPermission("railminer.reload")) {
                    sender.sendMessage(Messages.NO_PERMISSIONS.get());
                    return false;
                }

                RailMiner.getInstance().reloadConfig();
                sender.sendMessage(Messages.RELOAD_SUCCESS.get());
                return true;
            } else if (args[0].equalsIgnoreCase("upgrade") || args[0].equalsIgnoreCase("u")) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Only players can use this command!");
                    return false;
                }
                Player p = (Player) sender;
                if (!p.hasPermission("railminer.upgrades")) {
                    p.sendMessage(Messages.NO_PERMISSIONS.get());
                    return false;
                }

                if (!(args.length > 1)) {
                    p.sendMessage(Messages.INVALID_ARGS.get());
                    return false;
                }

                ItemStack i = new ItemStack(Material.ENCHANTED_BOOK);
                ItemMeta im = i.getItemMeta();

                YamlConfiguration c = upgrades.getYaml();
                if (args[1].equalsIgnoreCase("speed")) {
                    if (c.getString("recipes.speed.result") == null) {
                        RailMiner.getInstance().getLogger().severe("Please either regen your upgrades.yml or copy the new parts into from the resource page");
                        return false;
                    }
                    im.setDisplayName(ChatColor.translateAlternateColorCodes('&', c.getString("recipes.speed.result")));
                    i.setItemMeta(im);
                    p.getInventory().addItem(i);
                } else if (args[1].equalsIgnoreCase("fortune")) {
                    if (c.getString("recipes.fortune.result") == null) {
                        RailMiner.getInstance().getLogger().severe("Please either regen your upgrades.yml or copy the new parts into from the resource page");
                        return false;
                    }
                    im.setDisplayName(ChatColor.translateAlternateColorCodes('&', c.getString("recipes.fortune.result")));
                    i.setItemMeta(im);
                    p.getInventory().addItem(i);
                } else if (args[1].equalsIgnoreCase("storage")) {
                    if (c.getString("recipes.storage.result") == null) {
                        RailMiner.getInstance().getLogger().severe("Please either regen your upgrades.yml or copy the new parts into from the resource page");
                        return false;
                    }
                    im.setDisplayName(ChatColor.translateAlternateColorCodes('&', c.getString("recipes.storage.result")));
                    i.setItemMeta(im);
                    p.getInventory().addItem(i);
                } else if (args[1].equalsIgnoreCase("silk")) {
                    if (c.getString("recipes.silk.result") == null) {
                        RailMiner.getInstance().getLogger().severe("Please either regen your upgrades.yml or copy the new parts into from the resource page");
                        return false;
                    }
                    im.setDisplayName(ChatColor.translateAlternateColorCodes('&', c.getString("recipes.silk.result")));
                    i.setItemMeta(im);
                    p.getInventory().addItem(i);
                } else if (args[1].equalsIgnoreCase("efficiency")) {
                    if (c.getString("recipes.efficiency.result") == null) {
                        RailMiner.getInstance().getLogger().severe("Please either regen your upgrades.yml or copy the new parts into from the resource page");
                        return false;
                    }
                    im.setDisplayName(ChatColor.translateAlternateColorCodes('&', c.getString("recipes.efficiency.result")));
                    i.setItemMeta(im);
                    p.getInventory().addItem(i);
                } else if (args[1].equalsIgnoreCase("protection")) {
                    if (c.getString("recipes.protection.result") == null) {
                        RailMiner.getInstance().getLogger().severe("Please either regen your upgrades.yml or copy the new parts into from the resource page");
                        return false;
                    }
                    im.setDisplayName(ChatColor.translateAlternateColorCodes('&', c.getString("recipes.protection.result")));
                    i.setItemMeta(im);
                    p.getInventory().addItem(i);
                } else if (args[1].equalsIgnoreCase("storage_ii")) {
                    ItemStack result = new ItemStack(Material.ENCHANTED_BOOK);
                    ItemMeta rm = result.getItemMeta();
                    rm.setDisplayName(ChatColor.RED + "Storage II Upgrade");
                    result.setItemMeta(rm);
                    p.getInventory().addItem(result);
                } else {
                    p.sendMessage(ChatColor.RED + "Invalid upgrade! Select one of the following: speed, fortune, storage, storage_ii, silk or efficiency");
                }
                return true;
            } else {
                sender.sendMessage(Messages.INVALID_ARGS.get());
                return false;
            }
        }
        return false;
    }

}
