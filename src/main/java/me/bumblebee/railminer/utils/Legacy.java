package me.bumblebee.railminer.utils;

import org.bukkit.Material;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Legacy {
    private static Method GET_MATERIAL;
    static {
        try {
            GET_MATERIAL = Material.class.getDeclaredMethod("getMaterial", int.class);
        } catch (NoSuchMethodException e) {
            GET_MATERIAL = null;
        }
    }
    public Material getMaterial(Material m, int id){
        try {
            return GET_MATERIAL != null ? (Material) GET_MATERIAL.invoke(m,id) : null;
        } catch (IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }
}
