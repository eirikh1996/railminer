package me.bumblebee.railminer.utils;

import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.WallSign;

public class Sign {
    public static BlockFace getAttachedFace(WallSign sign){

        BlockFace signFace = sign.getFacing();
        switch (signFace){
            case WEST:
                return BlockFace.EAST;
            case EAST:
                return BlockFace.WEST;
            case NORTH:
                return BlockFace.SOUTH;
            case SOUTH:
                return BlockFace.NORTH;
        }
        return null;
    }
}
