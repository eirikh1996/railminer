package me.bumblebee.railminer.utils;

import me.bumblebee.railminer.RailMiner;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Friends {

    public static HashMap<UUID, List<UUID>> friends = new HashMap<>();

    public void addFriend(UUID key, UUID toAdd) {
        List<UUID> f;
        if (friends.containsKey(key)) {
            f = getFriends(key);
        } else {
            f = new ArrayList<>();
        }
        f.add(toAdd);
        friends.remove(key);
        friends.put(key, f);
    }

    public void delFriend(UUID key, UUID toAdd) {
        List<UUID> f = getFriends(key);
        f.remove(toAdd);
        friends.remove(key);
        friends.put(key, f);
    }

    public List<UUID> getFriends(UUID uuid) {
        return friends.get(uuid);
    }

    public boolean isFriendsWith(UUID key, UUID toCheck) {
        if (!friends.containsKey(key))
            return false;
        List<UUID> f = getFriends(key);
        if (f.contains(toCheck))
            return true;
        return false;
    }

    public void saveData() {
        YamlConfiguration c = getFile();
        for (UUID uuid : friends.keySet()) {
            ArrayList<String> uuids = new ArrayList<>();
            for (UUID t : friends.get(uuid))
                uuids.add(String.valueOf(t));
            c.set("friends." + uuid, uuids);
        }
        saveFile(c);
    }

    public void loadData() {
        YamlConfiguration c = getFile();
        if (c.getConfigurationSection("friends") == null)
            return;

        for (String s : c.getConfigurationSection("friends").getKeys(false)) {
            List<UUID> add = new ArrayList<>();
            for (String f : c.getStringList("friends." + s)) {
                add.add(UUID.fromString(f));
            }
            friends.put(UUID.fromString(s), add);
        }
    }

    public YamlConfiguration getFile() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "friends.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return YamlConfiguration.loadConfiguration(f);
    }

    public void saveFile(YamlConfiguration c) {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "friends.yml");
        if (!f.exists())
            return;
        try {
            c.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
