package me.bumblebee.railminer.utils;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownBlock;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class TownyProtectionTester {

    public static boolean test(Player owner, Block b) {
        boolean towny = true;
        TownBlock t = TownyUniverse.getTownBlock(b.getLocation());
        if (t != null) {
            Town town = null;
            Resident r = null;
            try {
                town = t.getTown();
                r = TownyUniverse.getDataSource().getResident(owner.getName());
            } catch (NotRegisteredException e) {
                e.printStackTrace();
            }

            Town pTown = null;
            if (r != null) {
                try {
                    pTown = r.getTown();
                } catch (NotRegisteredException e) {
                    towny = false;
                }

                if (pTown != null) {
                    if (!pTown.getName().equals(town.getName())) {
                        towny = false;
                    }
                } else {
                    towny = false;
                }
            }
        }
        return towny;
    }

}
