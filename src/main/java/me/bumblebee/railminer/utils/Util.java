package me.bumblebee.railminer.utils;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

public class Util {

    public boolean checkIfSignBroken(Block b) {
        for (BlockFace f : BlockFace.values()) {
            if (b.getRelative(f).getType() == Material.WALL_SIGN) {
                return true;
            }
        }
        return false;
    }
}
