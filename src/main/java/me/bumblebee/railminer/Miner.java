package me.bumblebee.railminer;

import br.net.fabiozumbi12.RedProtect.Bukkit.API.RedProtectAPI;
import br.net.fabiozumbi12.RedProtect.Bukkit.Region;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.massivecore.ps.PS;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import me.bumblebee.railminer.managers.LimitManager;
import me.bumblebee.railminer.managers.Messages;
import me.bumblebee.railminer.managers.UpgradeManager;
import me.bumblebee.railminer.upgrades.*;
import me.bumblebee.railminer.utils.*;
import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.block.Chest;
import org.bukkit.block.Furnace;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Waterlogged;
import org.bukkit.block.data.type.*;
import org.bukkit.block.data.type.Dispenser;
import org.bukkit.entity.*;
import org.bukkit.entity.minecart.StorageMinecart;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.block.data.Directional;
import org.bukkit.material.Lever;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import com.songoda.kingdoms.constants.kingdom.Kingdom;
import com.songoda.kingdoms.constants.land.Land;
import com.songoda.kingdoms.constants.land.SimpleChunkLocation;
import com.songoda.kingdoms.constants.player.KingdomPlayer;
import com.songoda.kingdoms.main.Kingdoms;
import com.songoda.kingdoms.manager.game.GameManagement;
import com.songoda.kingdoms.manager.game.LandManager;

import java.util.*;

public class Miner {

    Protect p = new Protect();
    LimitManager limits = new LimitManager();
    UpgradeManager upgrade = new UpgradeManager();

    //Block = sign (For both)
    public static HashMap<Block, Miner> miners = new HashMap<>();
    public static HashMap<UUID, List<Block>> playerMiners = new HashMap<>();

    //Base
    private boolean first = true;
    private Block b;
    public Block sign;
    private UUID owner;
    private boolean status = false;
    private boolean take = false;
    private int takeCounter = 0;
    private int boostCounter;
    private Location d1;
    private Location d2;
    private int blocksMoved = 0;

    //Util/Storage
    private List<ItemStack> mined = new ArrayList<>();
    private ItemStack[] chest = null;
    public List<UpgradeType> upgrades = new ArrayList<>();
    public List<ItemStack> rawUpgrades = new ArrayList<>();
    public Block upgradeBlock = null;
    private String[] signData = null;
    private ItemStack f1 = null;
    private ItemStack f2 = null;
    private ItemStack[] storageChest1 = null;
    private ItemStack[] storageChest2 = null;
    private Location storageChestLoc1 = null;
    private Location storageChestLoc2 = null;
    public boolean placeUpgrades = true;

    //Directions
    private String forward = "";
    private String back = "";
    private String left = "";
    private String right = "";
    private String direction = "";

    //Upgrade Settings
    public int speedSettingOne = 20;
    public int speedSettingTwo = 10;
    public int fuelRequired = 2;


    //Pass b as the sign, Start will fix it
    public Miner(Block b, UUID owner) {
        this.b = b;
        this.sign = b;
        this.owner = owner;
    }


    public void stop() {
        status = false;
        upgradeBlock.setMetadata("upgradeChest", new FixedMetadataValue(RailMiner.getInstance(), false));
        miners.remove(sign);
        List<Block> blocks = playerMiners.get(owner);
        blocks.remove(sign);
        if (blocks.size() > 0) {
            playerMiners.remove(owner);
            playerMiners.put(owner, blocks);
        }

        Location l = forward(forward(up(sign.getLocation())));
        if (!(l.getBlock().getType() == Material.LEVER)) return;
        Bukkit.getServer().getScheduler().runTaskLater(RailMiner.getInstance(), new Runnable() {
            @Override
            public void run() {
                BlockState state = l.getBlock().getState();
                Lever b = (Lever) state.getData();
                b.setPowered(false);
                state.setData(b);
                state.update();
            }
        }, 5);


        Location l1;
        Location l2;
        if (d1 == null) {
            l1 = storageChestLoc1;
            l2 = storageChestLoc2;
        } else {
            l1 = d1;
            l2 = d2;
        }

        if (l1 != null) {
            BlockState b1 = down(l1.clone()).getBlock().getState();
            BlockState b2 = down(l2.clone()).getBlock().getState();
            if (b1 == null || b2 == null)
                return;

            if (b1 instanceof Furnace) {
                Furnace f1 = (Furnace) down(l1.clone()).getBlock().getState();
                f1.setBurnTime((short) 0);
            }
            if (b2 instanceof Furnace) {
                Furnace f2 = (Furnace) down(l2.clone()).getBlock().getState();
                f2.setBurnTime((short) 0);
            }
        }

        //Make sure upgrades get put back in to the chest
        Chest uc = (Chest) upgradeBlock.getState();
        if (chest != null)
            uc.getInventory().setContents((ItemStack[]) rawUpgrades.toArray());
    }

    public void start() {
        if (limits.getPlayerMinerLimit(getOwner(owner)) != 0) {
            if (limits.getAmountRunning(owner) >= limits.getPlayerMinerLimit(getOwner(owner))) {
                getOwner(owner).sendMessage(Messages.MINER_LIMIT_REACHED.get().replace("%limit%", String.valueOf(limits.getPlayerBlockLimit(getOwner(owner)))));
                return;
            }
        }
        limits.updateLimitFor(owner, true);

        miners.put(sign, this);
        if (playerMiners.containsKey(owner)) {
            List<Block> miners = playerMiners.get(owner);
            miners.add(sign);
            playerMiners.remove(owner);
            playerMiners.put(owner, miners);
        } else {
            playerMiners.put(owner, new ArrayList<>(Arrays.asList(sign)));
        }

        status = true;
        direction = String.valueOf(back(b.getLocation()).getBlock().getFace(b));

        upgradeBlock = forward(forward(forward(forward(up(sign.getLocation()))))).getBlock();
        if (upgradeBlock.getState() instanceof Chest) {
            if (upgrades != null) {
                if (upgrades.size() > upgrade.getUpgradeLimit()) {
                    stop();
                    getOwner(owner).sendMessage(Messages.UPGRADE_LIMIT_REACHED.get().replace("%amount%", ""+upgrade.getUpgradeLimit()));

                    upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
                    Chest uc = (Chest) upgradeBlock.getState();
                    if (uc != null) {
                        if (!first) {
                            if (uc.getInventory().getContents().length > 0)
                                return;
                        }
                        for (ItemStack is : rawUpgrades) {
                            if (is != null && placeUpgrades)
                                uc.getBlockInventory().addItem(is);
                        }
                    }
                    return;
                }
            }
        }

        if (!checkShape(b)) {
            getOwner(owner).sendMessage(Messages.INCORRECT_SHAPE.get());
            upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
            Chest uc = (Chest) upgradeBlock.getState();
            if (uc != null) {
                if (!first) {
                    if (uc.getInventory().getContents().length > 0)
                        return;
                }
                for (ItemStack is : rawUpgrades) {
                    if (is != null && placeUpgrades)
                        uc.getBlockInventory().addItem(is);
                }
            }
            stop();
            return;
        }

        final int finalSpeedSettingTwo = speedSettingTwo;
        final int finalSpeedSettingOne = speedSettingOne;
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(RailMiner.getInstance(), new Runnable() {
            int i = 1;
            boolean boost = false;
            boolean dispenserToggle = true;
            boolean storageToggle = true;
            @Override
            public void run() {
                if (status) {
                    //Make sure shape is correct
                    boolean shape = checkShape(sign);
                    if (!shape) {
                        stop();
                        if (getOwner(owner) != null) {
                            getOwner(owner).sendMessage(Messages.INCORRECT_SHAPE.get());
                            upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
                            Chest uc = (Chest) upgradeBlock.getState();
                            if (uc != null) {
                                if (!first) {
                                    if (uc.getInventory().getContents().length > 0)
                                        return;
                                }
                                for (ItemStack is : rawUpgrades) {
                                    if (is != null && placeUpgrades)
                                        uc.getBlockInventory().addItem(is);
                                }
                            }
                        }
                        return;
                    }

                    //Update dispenser or chest locations
                    if (upgrades.contains(UpgradeType.STORAGE) || upgrades.contains(UpgradeType.STORAGE_II)) {
                        updateStorageChests(sign);
                    } else {
                        updateDispensers(sign);
                    }
                    //Check if we need a boost rail
//                    boost = false;
                    if (boostCounter % 8 == 0)
                        boost = true;

                    //Get locations to reference later
                    final Location tip = forward(forward(forward(forward(forward(forward(forward(forward(sign.getLocation()))))))));
                    Location control = forward(down(left(sign.getLocation())));
                    Player p = getOwner(owner);

                    //Make sure block infront is solid
                    if (!down(down(tip)).getBlock().getType().isSolid() && RailMiner.getInstance().getConfig().getBoolean("empty-spaces")) {
                        if (p != null) {
                            p.sendMessage(Messages.FOUND_EMPTY_SPACE.get());
                        }
                        stop();
                        upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
                        Chest uc = (Chest) upgradeBlock.getState();
                        if (uc != null) {
                            if (!first) {
                                if (uc.getInventory().getContents().length > 0)
                                    return;
                            }
                            for (ItemStack is : rawUpgrades) {
                                if (is != null && placeUpgrades)
                                    uc.getBlockInventory().addItem(is);
                            }
                        }
                        return;
                    }

                    //Check if miner has reached its block limit
                    int blockLimit = limits.getPlayerBlockLimit(p);
                    if (blockLimit > 0) {
                        if (getBlocksMoved() >= blockLimit) {
                            p.sendMessage(Messages.BLOCK_LIMIT_REACHED.get().replace("%limit%", String.valueOf(blockLimit)));
                            stop();
                            upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
                            Chest uc = (Chest) upgradeBlock.getState();
                            if (uc != null) {
                                if (!first) {
                                    if (uc.getInventory().getContents().length > 0)
                                        return;
                                }
                                for (ItemStack is : rawUpgrades) {
                                    if (is != null && placeUpgrades)
                                        uc.getBlockInventory().addItem(is);
                                }
                            }
                            return;
                        }
                    }

                    //Check if fuel is enabled
                    if (RailMiner.getInstance().getConfig().getBoolean("enable-fuel")) {
                        if (!checkFuel()) {
                            p.sendMessage(Messages.OUT_OF_FUEL.get());
                            stop();
                            upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
                            Chest uc = (Chest) upgradeBlock.getState();
                            if (uc != null) {
                                if (!first) {
                                    if (uc.getInventory().getContents().length > 0)
                                        return;
                                }
                                for (ItemStack is : rawUpgrades) {
                                    if (is != null && placeUpgrades)
                                        uc.getBlockInventory().addItem(is);
                                }
                            }
                            return;
                        }
                    }
                    //Check if recipes are enabled
                    if (RailMiner.getInstance().getConfig().getBoolean("enable-recipes")) {
                        String[] recipe = checkRecipes(sign, boost).split(":");
                        if (!recipe[0].equalsIgnoreCase("noErrors")) {
                            p.sendMessage(Messages.REQUIRES_ITEM.get().replace("%item%", recipe[1]));
                            stop();
                            upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
                            Chest uc = (Chest) upgradeBlock.getState();
                            if (uc != null) {
                                if (!first) {
                                    if (uc.getInventory().getContents().length > 0)
                                        return;
                                }
                                for (ItemStack is : rawUpgrades) {
                                    if (is != null && placeUpgrades)
                                        uc.getBlockInventory().addItem(is);
                                }
                            }
                            return;
                        }
                        removeRecipes(sign, boost);
                    }
                    if (upgrades.contains(UpgradeType.LIQUID_PROTECTION))
                        checkForLiquid();

                    final boolean firstAgain = first;
                    new BukkitRunnable() {
                        int c = 1;
                        @Override
                        public void run() {
                            Bukkit.getServer().getScheduler().runTaskLater(RailMiner.getInstance(), new Runnable() {
                                @Override
                                public void run() {
                                    if (status) {
                                        //Remove blocks from around tip
                                        cleanBit(tip.getBlock());
                                        //Mine and make sure it was successful then toggle pistons
                                        boolean mine = mine(tip.getBlock(), c - 1);

                                        if (!mine) {
                                            stop();
                                            getOwner(owner).sendMessage(Messages.FAILED_TO_MINE.get());
                                            upgradeBlock = forward(forward(forward(forward(up(sign.getLocation().clone()))))).getBlock();
                                            Chest uc = (Chest) upgradeBlock.getState();
                                            if (uc != null) {
                                                if (!firstAgain) {
                                                    if (uc.getInventory().getContents().length > 0)
                                                        return;
                                                }
                                                for (ItemStack is : rawUpgrades) {
                                                    if (is != null && placeUpgrades)
                                                        uc.getBlockInventory().addItem(is);
                                                }
                                            }
                                        } else {
                                            tip.getWorld().spawnParticle(Particle.EXPLOSION_HUGE,tip,100);
                                            //ParticleEffect.EXPLOSION_HUGE.display(1, 1, 1, 1, 1, up(tip.clone()), 100);
                                            togglePistons(sign);
                                        }
                                    }
                                }
                            }, finalSpeedSettingTwo);

                            Bukkit.getServer().getScheduler().runTaskLater(RailMiner.getInstance(), new Runnable() {
                                @Override
                                public void run() {
                                    if (status) {
                                        togglePistons(sign);
                                    }
                                }
                            }, finalSpeedSettingOne);
                            if (c == 3) cancel();
                            c++;
                        }
                    }.runTaskTimer(RailMiner.getInstance(), 0, finalSpeedSettingOne);

                    if (!first) {
                        if (sign.getLocation().getBlock().getState() instanceof org.bukkit.block.Sign) {
                            miners.remove(sign);
                            List<Block> blocks = playerMiners.get(owner);
                            blocks.remove(sign);

                            updateRegion();
                            destroy(control.getBlock());
                            sign = forward(sign.getLocation()).getBlock();
                            control = forward(down(left(sign.getLocation())));
                            build(control.getBlock());

                            if (upgrades.contains(UpgradeType.STORAGE) || upgrades.contains(UpgradeType.STORAGE_II)) {
                                updateStorageChests(sign);
                                Furnace fu1 = (Furnace) storageChestLoc1.clone().subtract(0, 1, 0).getBlock().getState();
                                fu1.setBurnTime((short) 6000);
                                Furnace fu2 = (Furnace) storageChestLoc2.clone().subtract(0, 1, 0).getBlock().getState();
                                fu2.setBurnTime((short) 6000);
                                storageChestLoc1.getWorld().spawnParticle(Particle.LAVA,storageChestLoc1,10);
                                storageChestLoc2.getWorld().spawnParticle(Particle.LAVA,storageChestLoc2,10);
                                //ParticleEffect.LAVA.display(1, 1, 1, 1, 5, storageChestLoc1.clone(), 100);
                                //ParticleEffect.LAVA.display(1, 1, 1, 1, 5, storageChestLoc2.clone(), 100);

                                List<ItemStack> remove = new ArrayList<>();
                                for (ItemStack i : mined) {
                                    boolean storage = addToStorage(i, storageToggle);
                                    if (!storage) {
                                        if (upgrades.contains(UpgradeType.STORAGE_II)) {
                                            Location l = forward(up(sign.getLocation().clone()));
                                            Chest store = (Chest) l.getBlock().getState();
                                            if (store != null) {
                                                Inventory inv = store.getInventory();
                                                if (!inv.containsAtLeast(new ItemStack(Material.CHEST_MINECART), 1)) {
                                                    getOwner(owner).sendMessage(Messages.STORAGE_FULL.get());
                                                    stop();
                                                    return;
                                                }
                                                inv.removeItem(new ItemStack(Material.CHEST_MINECART, 1));

                                                Location spawn = down(sign.getLocation());
                                                StorageMinecart minecart = (StorageMinecart) spawn.getWorld().spawnEntity(spawn, EntityType.MINECART_CHEST);
                                                if (storageToggle) {
                                                    minecart.getInventory().setStorageContents(storageChest1);
                                                    storageChest1 = null;
                                                    Chest c =(Chest) storageChestLoc1.getBlock().getState();
                                                    for (ItemStack is : c.getInventory().getContents()) {
                                                        c.getInventory().removeItem(is);
                                                    }
                                                } else {
                                                    minecart.getInventory().setStorageContents(storageChest2);
                                                    storageChest2 = null;
                                                    Chest c =(Chest) storageChestLoc2.getBlock().getState();
                                                    for (ItemStack is : c.getInventory().getContents()) {
                                                        c.getInventory().removeItem(is);
                                                    }
                                                }
                                            }
                                        } else {
                                            getOwner(owner).sendMessage(Messages.STORAGE_FULL.get());
                                            stop();
                                            return;
                                        }
                                    }
                                    remove.add(i);
                                    storageToggle = !(storageToggle);
                                }
                                mined.removeAll(remove);
                            } else {
                                updateDispensers(sign);
                                Furnace fu1 = (Furnace) d1.clone().subtract(0, 1, 0).getBlock().getState();
                                fu1.setBurnTime((short) 6000);
                                Furnace fu2 = (Furnace) d2.clone().subtract(0, 1, 0).getBlock().getState();
                                fu2.setBurnTime((short) 6000);

                                d1.getWorld().spawnParticle(Particle.LAVA,d1,10);
                                d2.getWorld().spawnParticle(Particle.LAVA,d2,10);
                                //ParticleEffect.LAVA.display(1, 1, 1, 1, 5, d1.clone(), 100);
                                //ParticleEffect.LAVA.display(1, 1, 1, 1, 5, d2.clone(), 100);

                                List<ItemStack> remove = new ArrayList<>();
                                for (ItemStack i : mined) {
                                    addToDispensers(i, dispenserToggle);
                                    remove.add(i);
                                    dispenserToggle = !(dispenserToggle);
                                }
                                mined.removeAll(remove);
                            }

                            miners.put(sign, Miner.this);
                            blocks.add(sign);
                            playerMiners.remove(owner);
                            playerMiners.put(owner, blocks);

                            road(tip.getBlock(), boost);
                            boost = false;
                        } else {
                            stop();
                            getOwner(owner).sendMessage(Messages.FAILED_TO_MINE.get());
                            return;
                        }
                    } else {
                        first = false;
                        boost = true;
                    }
                    boostCounter++;
                    i++;
                    blocksMoved++;
                } else {
                    setSignStatus(sign, "Start");
                }
            }
        }, 20, (speedSettingOne*3)+10);
    }

    public void updateRegion() {
        ProtectedRegion r = p.getRegion(this);
        if (r == null)
            return;
        Location p1 = r.getLocationOne();
        Location p2 = r.getLocationTwo();
        r.remove();
        new ProtectedRegion(owner, forward(p1), forward(p2));
    }

    public boolean checkFuel() {
        Location s = sign.getLocation();
        Location l1 = forward(left(s.clone()));
        Location l2 = forward(right(s.clone()));

        Furnace f1 = (Furnace) l1.clone().subtract(0,1,0).getBlock().getState();
        Furnace f2 = (Furnace) l2.clone().subtract(0,1,0).getBlock().getState();

        if (f1.getInventory().getFuel() == null)
            return false;
        if (f1.getInventory().getFuel().getType() == Material.COAL) {
            if (!(f1.getInventory().getFuel().getAmount() >= fuelRequired))
                return false;
        } else if (f1.getInventory().getFuel().getType() == Material.COAL_BLOCK) {
            double required = fuelRequired/9;
            if (!(f1.getInventory().getFuel().getAmount() >= required))
                return false;
            } else {
            return false;
        }

        if (f2.getInventory().getFuel() == null)
            return false;
        if (f2.getInventory().getFuel().getType() == Material.COAL) {
            if (!(f2.getInventory().getFuel().getAmount() >= fuelRequired))
                return false;
        } else if (f2.getInventory().getFuel().getType() == Material.COAL_BLOCK) {
            double required = fuelRequired/9;
            if (!(f2.getInventory().getFuel().getAmount() >= required))
                return false;
        } else {
            return false;
        }
        return true;
    }

    public ItemStack changeMaterial(Block m) {
        if (upgrades.contains(UpgradeType.SILK_TOUCH))
            return new SilkTouchUpgrade(m.getType()).run();

        List<String> change = RailMiner.getInstance().getConfig().getStringList("change-drops");
        for (String raw : change) {
            try {
                String[] s = raw.split("=");
                String[] one = s[0].split(":");
                String[] two = s[1].split("x")[0].split(":");

                if (Material.getMaterial(one[0]) != m.getType()) {
                    continue;
                }
                if (!(one.length > 1)) {
                    ItemStack i = new ItemStack(Material.getMaterial(two[0]));
                    if (two.length > 1) {
                        i.setDurability((short) Integer.parseInt(two[1]));
                    }

                    String[] multi = s[1].split("x");
                    if (multi.length > 1) {
                        int amount = Integer.parseInt(multi[1]);
                        i.setAmount(amount);
                    }

                    return i;
                }



                ItemStack i = new ItemStack(Material.getMaterial(two[0]));
                if (two.length > 1) {
                    i.setDurability((short) Integer.parseInt(two[1]));
                }

                String[] multi = s[1].split("x");
                if (multi.length > 1) {
                    int amount = Integer.parseInt(multi[1]);
                    i.setAmount(amount);
                }

                return i;
            } catch (Exception e) {
                RailMiner.getInstance().getLogger().warning("Failed to read change-drops! Is it formatted correctly?");
                e.printStackTrace();
                RailMiner.getInstance().getLogger().warning("Stopping all miners and disabling plugin..");
                RailMiner.stopAllMiners();
                Bukkit.getServer().getPluginManager().disablePlugin(RailMiner.getInstance());
            }
        }
        return new ItemStack(m.getType());
    }

    public int getBlocksMoved() { return blocksMoved; }

    public boolean checkBlacklist(Block b) {
        List<String> blocks = RailMiner.getInstance().getConfig().getStringList("block-blacklist");
        if (blocks.size() == 0)
            return true;
        for (String id : blocks) {
            String[] split = id.split(":");
            if (b.getType() == Material.getMaterial(split[0])) {
                if (split.length > 1) {
                    if (b.getData() == Integer.parseInt(split[1])) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public void setSignStatus(Block b, String status) {
        if (!(b.getState() instanceof org.bukkit.block.Sign)) return;
        org.bukkit.block.Sign sign = (org.bukkit.block.Sign) b.getState();
        if (signData == null) {
            signData = sign.getLines();
            signData = new String[]{"[RailMiner]", status};
        } else {
            signData[1] = status;
        }
        org.bukkit.block.Sign s = (org.bukkit.block.Sign) b.getState();
        if (s != null) {
            s.setLine(1, status);
            s.update();
        }
    }

    //Pass s as sign
    public String checkRecipes(Block s, boolean boost) {
        Chest c = getChest(s);
        if (c == null) return "error:null";

        Inventory inv = c.getInventory();

        if (boost) {
            if (!inv.containsAtLeast(new ItemStack(Material.STONE_BRICKS), 9)) return "error:stone brick";
            if (!inv.containsAtLeast(new ItemStack(Material.IRON_INGOT), 6)) return "error:iron ingot";
            if (!inv.containsAtLeast(new ItemStack(Material.STICK), 4)) return "error:stick";
            if (!inv.containsAtLeast(new ItemStack(Material.REDSTONE), 2)) return "error:redstone";
            if (!inv.containsAtLeast(new ItemStack(Material.COAL), 1)) return "error:coal";
            if (!inv.containsAtLeast(new ItemStack(Material.GOLD_INGOT), 6)) return "error:gold ingot";
        } else {
            if (!inv.containsAtLeast(new ItemStack(Material.STONE_BRICKS), 3)) return "error:stone brick";
            if (!inv.containsAtLeast(new ItemStack(Material.IRON_INGOT), 6)) return "error:iron ingot";
            if (!inv.containsAtLeast(new ItemStack(Material.STICK), 1)) return "error:stick";
        }

        return "noErrors:none";
    }

    //checkRecipes() should be ran first
    public void removeRecipes(Block s, boolean boost) {
        Chest c = getChest(s);
        Inventory inv = c.getInventory();

        if (boost) {
            inv.removeItem(new ItemStack(Material.STONE_BRICKS, 9));
            inv.removeItem(new ItemStack(Material.IRON_INGOT, 6));
            inv.removeItem(new ItemStack(Material.STICK, 4));
            inv.removeItem(new ItemStack(Material.REDSTONE, 2));
            inv.removeItem(new ItemStack(Material.COAL, 1));
            inv.removeItem(new ItemStack(Material.GOLD_INGOT, 6));
        } else {
            inv.removeItem(new ItemStack(Material.STONE_BRICKS, 3));
            inv.removeItem(new ItemStack(Material.STICK, 1));
        }
    }

    public Chest getChest(Block s) {
        Block b = forward(up(s.getLocation())).getBlock();
        if (!(b.getState() instanceof Chest)) {
            return null;
        }
        return ((Chest) b.getState());
    }

    public void addToDispensers(ItemStack i, boolean dispenserToggle) {
        if (dispenserToggle) {
            InventoryHolder ih1 = (InventoryHolder) d1.clone().getBlock().getState();
            ih1.getInventory().addItem(i);
        } else {
            InventoryHolder ih2 = (InventoryHolder) d2.clone().getBlock().getState();
            ih2.getInventory().addItem(i);
        }
    }

    public boolean addToStorage(ItemStack i, boolean storageToggle) {
        if (storageToggle) {
            Chest c1 = (Chest) storageChestLoc1.clone().getBlock().getState();
            int amount = 0;
            for (ItemStack item : c1.getInventory().getContents()) {
                if (item == null || item.getType() == Material.AIR)
                    continue;
                if (i.getType() == item.getType())
                    continue;
                amount++;
            }

            if (amount >= 27)
                return false;
            c1.getInventory().addItem(i);
        } else {
            Chest c2 = (Chest) storageChestLoc2.clone().getBlock().getState();
            int amount = 0;
            for (ItemStack item : c2.getInventory().getContents()) {
                if (item == null || item.getType() == Material.AIR)
                    continue;
                if (i.getType() == item.getType())
                    continue;
                amount++;
            }

            if (amount >= 27)
                return false;
            c2.getInventory().addItem(i);
        }
        return true;
    }

    //Pass s as sign
    public void updateDispensers(Block s) {
        Location l = s.getLocation();
        l = forward(left(l));
        if (l.getBlock().getType() != Material.DISPENSER)  {
            RailMiner.getInstance().getLogger().warning("Failed to get dispensers on " + owner + "'s miner!");
            return;
        }
        d1 = l;

        Location l2 = right(right(l.clone()));
        if (l2.getBlock().getType() != Material.DISPENSER)  {
            RailMiner.getInstance().getLogger().warning("Failed to get dispensers on " + owner + "'s miner!");
            return;
        }
        d2 = l2;
    }

    //Pass s as sign
    public void updateStorageChests(Block s) {
        Location l = s.getLocation();
        l = forward(left(l));
        if (l.getBlock().getType() != Material.CHEST)  {
            RailMiner.getInstance().getLogger().warning("Failed to get dispensers on " + owner + "'s miner!");
            return;
        }
        storageChestLoc1 = l;

        Location l2 = right(right(l.clone()));
        if (l2.getBlock().getType() != Material.CHEST)  {
            RailMiner.getInstance().getLogger().warning("Failed to get dispensers on " + owner + "'s miner!");
            return;
        }
        storageChestLoc2 = l2;
    }


    //Pass s as sign
    public void togglePistons(Block s) {
        Location l = s.getLocation();
        l = forward(forward(up(l)));
        if (!(l.getBlock().getType() == Material.LEVER)) return;

        Block b = l.getBlock();
        Switch data = (Switch) b.getBlockData();
        if (data.isPowered()){
            data.setPowered(false);
        } else {
            data.setPowered(true);
        }
        b.setBlockData(data);
    }

    public void checkForLiquid() {
        Location center = forward(forward(forward(forward(forward(forward(forward(forward(sign.getLocation().clone()))))))));
        Location div = forward(forward(forward(sign.getLocation().clone())));

        Location centerLeft = left(left(center.clone()));
        Location centerTop = up(up(center.clone()));
        Location centerRight = right(right(center.clone()));

        //Left (Actually right)
        if (checkWater(centerLeft.getBlock()) || checkLava(centerLeft.getBlock())) {
            centerLeft.getBlock().setType(Material.GLASS);
            up(centerLeft.clone()).getBlock().setType(Material.GLASS);
            down(centerLeft.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(up(centerLeft.clone()).getBlock()) || checkLava(up(centerLeft.clone()).getBlock())) {
            centerLeft.getBlock().setType(Material.GLASS);
            up(centerLeft.clone()).getBlock().setType(Material.GLASS);
            down(centerLeft.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(down(centerLeft.clone()).getBlock()) || checkLava(down(centerLeft.clone()).getBlock())) {
            centerLeft.getBlock().setType(Material.GLASS);
            up(centerLeft.clone()).getBlock().setType(Material.GLASS);
            down(centerLeft.clone()).getBlock().setType(Material.GLASS);
        }

        //Right (Actually left)
        if (checkWater(centerRight.clone().getBlock()) || checkLava(centerRight.clone().getBlock())) {
            centerRight.getBlock().setType(Material.GLASS);
            up(centerRight.clone()).getBlock().setType(Material.GLASS);
            down(centerRight.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(up(centerRight.clone()).getBlock()) || checkLava(up(centerRight.clone()).getBlock())) {
            centerRight.getBlock().setType(Material.GLASS);
            up(centerRight.clone()).getBlock().setType(Material.GLASS);
            down(centerRight.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(down(centerRight.clone()).getBlock()) || checkLava(down(centerRight.clone()).getBlock())) {
            centerRight.getBlock().setType(Material.GLASS);
            up(centerRight.clone()).getBlock().setType(Material.GLASS);
            down(centerRight.clone()).getBlock().setType(Material.GLASS);
        }

        //Top
        if (checkWater(centerTop.clone().getBlock()) || checkLava(centerTop.clone().getBlock())) {
            centerTop.getBlock().setType(Material.GLASS);
            left(centerTop.clone()).getBlock().setType(Material.GLASS);
            right(centerTop.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(left(centerLeft.clone()).getBlock()) || checkLava(left(centerLeft.clone()).getBlock())) {
            centerTop.getBlock().setType(Material.GLASS);
            left(centerTop.clone()).getBlock().setType(Material.GLASS);
            right(centerTop.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(right(centerLeft.clone()).getBlock()) || checkLava(right(centerLeft.clone()).getBlock())) {
            centerTop.getBlock().setType(Material.GLASS);
            left(centerTop.clone()).getBlock().setType(Material.GLASS);
            right(centerTop.clone()).getBlock().setType(Material.GLASS);
        }

        Location divTop = up(up(div.clone()));

        //Top
        if (checkWater(divTop.clone().getBlock()) || checkLava(divTop.clone().getBlock())) {
            divTop.getBlock().setType(Material.GLASS);
            left(divTop.clone()).getBlock().setType(Material.GLASS);
            right(divTop.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(left(divTop.clone()).getBlock()) || checkLava(left(divTop.clone()).getBlock())) {
            divTop.getBlock().setType(Material.GLASS);
            left(divTop.clone()).getBlock().setType(Material.GLASS);
            right(divTop.clone()).getBlock().setType(Material.GLASS);
        }
        if (checkWater(right(divTop.clone()).getBlock()) || checkLava(right(divTop.clone()).getBlock())) {
            divTop.getBlock().setType(Material.GLASS);
            left(divTop.clone()).getBlock().setType(Material.GLASS);
            right(divTop.clone()).getBlock().setType(Material.GLASS);
        }
    }

    public void destroy(Block s) {
        Location l = s.getLocation();
        //Layer 1
        Furnace furnace1 = (Furnace) l.getBlock().getState();
        f1 = furnace1.getInventory().getFuel();
        furnace1.getInventory().setFuel(new ItemStack(Material.AIR));
        s.setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        Furnace furnace2 = (Furnace) l.getBlock().getState();
        f2 = furnace2.getInventory().getFuel();
        furnace2.getInventory().setFuel(new ItemStack(Material.AIR));
        l.getBlock().setType(Material.AIR);
        l = forward(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = forward(up(l));
        l.getBlock().setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = down(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = forward(right(right(l)));
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = left(up(l));
        l.getBlock().setType(Material.AIR);
        l = down(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = forward(l);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = forward(left(l));
        l.getBlock().setType(Material.AIR);

        //Layer 2
        // D
        //DPD
        //PIP
        //IRI
        //RRR
        //III
        //DID
        l = up(l);
        l.getBlock().setType(Material.AIR);
        l = forward(l);
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = back(right(l));
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = left(left(l));
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = back(l);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = back(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = up(back(back(l)));
        l.getBlock().setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        l.getBlock().setType(Material.AIR);
        l = down(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        //Chest/Dispenser 1
        l = back(l);
        if (l.getBlock().getType() == Material.CHEST) {
            Chest c = (Chest) l.getBlock().getState();
            storageChest1 = c.getInventory().getContents();
            for (ItemStack is : c.getInventory().getContents()) {
                if (is != null) {
                    c.getInventory().remove(is);
                }
            }
        }
        l.getBlock().setType(Material.AIR);
        //CHEST
        l = right(up(l));
        if (l.getBlock().getType() == Material.CHEST) {
            Chest c = (Chest) l.getBlock().getState();
            chest = c.getInventory().getContents();
            for (ItemStack is : c.getInventory().getContents()) {
                if (is != null) {
                    c.getInventory().remove(is);
                }
            }
        }
        l.getBlock().setType(Material.AIR);
        //SIGN
        l = back(down(l));
        org.bukkit.block.Sign si = (org.bukkit.block.Sign) l.getBlock().getState();
        signData = si.getLines();
        l.getBlock().setType(Material.AIR);
        l = forward(l);
        l.getBlock().setType(Material.AIR);
        //Chest/Dispenser 2
        l = right(l);
        if (l.getBlock().getType() == Material.CHEST) {
            Chest c = (Chest) l.getBlock().getState();
            storageChest2 = c.getInventory().getContents();
            for (ItemStack is : c.getInventory().getContents()) {
                if (is != null) {
                    c.getInventory().remove(is);
                }
            }
        }
        l.getBlock().setType(Material.AIR);

        //Layer 3
        // D
        //DPD
        //PIP
        //III
        //RLR
        // C
        l = forward(forward(up(l)));
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = forward(l);
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = right(l);
        if (l.getBlock().getType() == Material.CHEST) {
            Chest c = (Chest) l.getBlock().getState();
            boolean updateUpdates = rawUpgrades.isEmpty();
            if (updateUpdates)
                rawUpgrades = Arrays.asList(c.getInventory().getContents());
            for (ItemStack is : c.getInventory().getContents()) {
                if (is == null)
                    continue;
                if (!is.hasItemMeta()) {
                    getOwner(owner).sendMessage(Messages.INVALID_UPGRADE.get());
                    placeUpgrades = false;
                    stop();
                    return;
                }
                if (!is.getItemMeta().hasDisplayName()) {
                    getOwner(owner).sendMessage(Messages.INVALID_UPGRADE.get());
                    placeUpgrades = false;
                    stop();
                    return;
                }

                String dis = is.getItemMeta().getDisplayName();
                if (!upgrade.checkDisplayName(dis)) {
                    getOwner(owner).sendMessage(Messages.INVALID_UPGRADE.get());
                    placeUpgrades = false;
                    stop();
                    return;
                }

                UpgradeType ut;
                String[] data = ChatColor.stripColor(dis).split(" ");
                if (data.length > 2) {
                    ut = UpgradeType.fromString(data[0] + "_" + data[1]);
                } else {
                    ut = UpgradeType.fromString(data[0]);
                    if (ut == null)
                        continue;
                }
                c.getInventory().removeItem(is);

                if (updateUpdates)
                    upgrades.add(ut);
            }
        }
        l.getBlock().setType(Material.AIR);
        l = right(l);
        if (forward(l.clone()).getBlock().getType() == Material.PISTON_HEAD)
            forward(l.clone()).getBlock().setType(Material.AIR);
        l.getBlock().setType(Material.AIR);
        l = forward(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = left(l);
        l.getBlock().setType(Material.AIR);
        l = forward(right(l));
        l.getBlock().setType(Material.AIR);
    }

    public void build(Block s) {
        take = !take;
        int fuelModifier = fuelRequired == 2 ? 1 : 2;
        Location l = s.getLocation();
        //Layer 1
        // D
        //DPD
        //PIP
        //III
        //III
        //FIF
        s.setType(Material.FURNACE);
        l.getBlock().setBlockData(getDirection("backwards-f"));
        Furnace furnace1 = (Furnace) l.getBlock().getState();
        if (f1 != null) {
            if (take) {
                if (f1.getType() == Material.COAL_BLOCK && takeCounter >= (9*fuelModifier))
                    f1.setAmount(f1.getAmount() - fuelRequired);
                else if (f1.getType() != Material.COAL_BLOCK)
                    f1.setAmount(f1.getAmount() - fuelRequired);
                takeCounter++;
            }
            furnace1.getInventory().setFuel(f1);
        }
        l = right(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = right(l);
        l.getBlock().setType(Material.FURNACE);
        l.getBlock().setBlockData(getDirection("backwards-f"));
        Furnace furnace2 = (Furnace) l.getBlock().getState();
        if (f2 != null) {
            if (take) {
                if (f2.getType() == Material.COAL_BLOCK && takeCounter >= (9*fuelModifier))
                    f2.setAmount(f2.getAmount() - fuelRequired);
                else if (f2.getType() != Material.COAL_BLOCK)
                    f2.setAmount(f2.getAmount() - fuelRequired);
                takeCounter++;
            }
            furnace2.getInventory().setFuel(f2);
        }
        l = forward(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = forward(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = right(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = right(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = forward(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = forward(l);
        l.getBlock().setType(Material.DIAMOND_BLOCK);
        l = right(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = right(l);
        l.getBlock().setType(Material.DIAMOND_BLOCK);
        l = forward(left(l));
        l.getBlock().setType(Material.DIAMOND_BLOCK);

        //Layer 2
        // D
        //DPD
        //PIP
        //IRI
        //RRR
        //III
        //DID
        l = up(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = forward(l);
        l.getBlock().setType(Material.DIAMOND_BLOCK);
        l = back(right(l));
        l.getBlock().setType(Material.DIAMOND_BLOCK);
        l = left(left(l));
        l.getBlock().setType(Material.DIAMOND_BLOCK);
        l = back(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = right(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = right(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = back(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.REDSTONE_WIRE);
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = back(l);
        l.getBlock().setType(Material.REPEATER);
        l.getBlock().setBlockData(getDirection("diode"));
        l = right(l);
        l.getBlock().setType(Material.REDSTONE_WIRE);
        l = right(l);
        l.getBlock().setType(Material.REPEATER);
        l.getBlock().setBlockData(getDirection("diode"));
        l = back(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = back(l);
        if (upgrades.contains(UpgradeType.STORAGE) || upgrades.contains(UpgradeType.STORAGE_II)) {
            l.getBlock().setType(Material.CHEST);
            if (storageChest1 != null) {
                Chest c = (Chest) l.getBlock().getState();
                c.getInventory().setContents(storageChest1);

            }
            l.getBlock().setBlockData(getDirection("backwards-c"));
        } else {
            l.getBlock().setType(Material.DISPENSER);
            l.getBlock().setBlockData(getDirection("backwards-d"));
        }


        l = right(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = right(l);
        if (upgrades.contains(UpgradeType.STORAGE) || upgrades.contains(UpgradeType.STORAGE_II)) {
            l.getBlock().setType(Material.CHEST);
            if (storageChest2 != null) {
                Chest c = (Chest) l.getBlock().getState();
                c.getInventory().setContents(storageChest2);
            }
            l.getBlock().setBlockData(getDirection("backwards-c"));
        } else {
            l.getBlock().setType(Material.DISPENSER);
            l.getBlock().setBlockData(getDirection("backwards-d"));
        }
        //SIGN
        l = back(left(l));
        l.getBlock().setType(Material.WALL_SIGN);
        l.getBlock().setBlockData(getDirection("backwards-s"));

        org.bukkit.block.Sign si = (org.bukkit.block.Sign) l.getBlock().getState();
        for (int i = 0; i < signData.length; i++) {
            si.setLine(i, signData[i]);
        }
        if (status) {
            si.setLine(1, "Stop");
        } else {
            si.setLine(1, "Stop");
        }
        si.update();
        l = forward(right(l));

        //Layer 3
        // D
        //DPD
        //PIP
        //III
        //RLR
        // C
        l = up(left(l));
        l.getBlock().setType(Material.CHEST);
        l.getBlock().setBlockData(getDirection("backwards-c"));
        Chest c = (Chest) l.getBlock().getState();
        if (chest != null) {
            for (ItemStack is : chest) {
                if (is != null) {
                    c.getBlockInventory().addItem(is);
                }
            }
        }
        l.getBlock().setMetadata("upgradeChest", new FixedMetadataValue(RailMiner.getInstance(), false));
        l = forward(left(l));
        l.getBlock().setType(Material.REDSTONE_WIRE);
        l = right(l);
        l.getBlock().setType(Material.LEVER);
        l.getBlock().setBlockData(getDirection("lever"));
        l = right(l);
        l.getBlock().setType(Material.REDSTONE_WIRE);
        l = forward(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.IRON_BLOCK);
        l = forward(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = right(l);
        l.getBlock().setType(Material.CHEST);
        upgradeBlock = l.getBlock();
        Chest uc = (Chest) l.getBlock().getState();
        if (chest != null) {
            for (ItemStack is : rawUpgrades) {
                if (is != null && placeUpgrades)
                    uc.getBlockInventory().addItem(is);
            }
        }
        if (l.getBlock().getLocation().equals(upgradeBlock.getLocation())) {
            l.getBlock().setMetadata("upgradeChest", new FixedMetadataValue(RailMiner.getInstance(), status));
        }
        l = right(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = forward(l);
        l.getBlock().setType(Material.DIAMOND_BLOCK);
        l = left(l);
        l.getBlock().setType(Material.STICKY_PISTON);
        l.getBlock().setBlockData(getDirection("piston"));
        l = left(l);
        l.getBlock().setType(Material.DIAMOND_BLOCK);
        l = forward(right(l));
        l.getBlock().setType(Material.DIAMOND_BLOCK);
    }

    public boolean cleanBit(Block tip) {
        Location l = back(up(up(tip.getLocation())));
        if (l.getBlock().getType() == Material.STICKY_PISTON || l.getBlock().getType() == Material.PISTON_HEAD) {
            l = forward(l);
        }

        l = down(l);
        l.getBlock().setType(Material.AIR);
        l = back(left(l));
        l.getBlock().setType(Material.AIR);
        l = right(right(l));
        l.getBlock().setType(Material.AIR);

        l = up(left(forward(forward(l))));
        l.getBlock().setType(Material.AIR);
        l = back(left(l));
        l.getBlock().setType(Material.AIR);
        l = right(right(l));
        l.getBlock().setType(Material.AIR);

        l = left(up(l));
        l.getBlock().setType(Material.AIR);
        l = back(left(l));
        l.getBlock().setType(Material.AIR);
        l = right(right(l));
        l.getBlock().setType(Material.AIR);
        return true;
    }

    public boolean mine(Block tip, int level) {
        Location l = up(up(tip.getLocation()));
        Player p = getOwner(owner);
        if (p == null) return true;
        boolean build = canBuild(p, l.getBlock());
        int multiplier;
        ItemStack i;

        if (level == 1) {
            l = forward(down(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            for (Entity e : l.getWorld().getNearbyEntities(l, 2, 2, 2)) {
                if (e instanceof Player) {
                    ((Player) e).damage(2);
                } else {
                    if (e instanceof LivingEntity) {
                        ((LivingEntity) e).damage(2);
                    }
                }
            }

            if (checkBlacklist(l.getBlock())) {
                return false;
            }

            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);

            l = back(left(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            if (checkBlacklist(l.getBlock())) {
                return false;
            }

            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);

            l = right(right(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            if (checkBlacklist(l.getBlock())) {
                return false;
            }
            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);
        } else if (level == 2) {
            l = forward(forward(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            for (Entity e : l.getWorld().getNearbyEntities(l, 2, 2, 2)) {
                if (e instanceof Player) {
                    ((Player) e).damage(2);
                } else {
                    if (e instanceof LivingEntity) {
                        ((LivingEntity) e).damage(2);
                    }
                }
            }

            if (checkBlacklist(l.getBlock())) {
                return false;
            }

            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);
            l = back(left(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            if (checkBlacklist(l.getBlock())) {
                return false;
            }
            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);
            l = right(right(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            if (checkBlacklist(l.getBlock())) {
                return false;
            }
            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);
        } else if (level == 3) {
            l = forward(up(l));
            if (!build) {
                return false;
            }
            for (Entity e : l.getWorld().getNearbyEntities(l, 2, 2, 2)) {
                if (e instanceof Player) {
                    ((Player) e).damage(2);
                } else {
                    if (e instanceof LivingEntity) {
                        ((LivingEntity) e).damage(2);
                    }
                }
            }

            if (checkBlacklist(l.getBlock())) {
                return false;
            }
            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);
            l = back(left(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            if (checkBlacklist(l.getBlock())) {
                return false;
            }
            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);
            l = right(right(l));
            build = canBuild(p, l.getBlock());
            if (!build) {
                return false;
            }
            if (checkBlacklist(l.getBlock())) {
                return false;
            }
            i = changeMaterial(l.getBlock());
            multiplier = upgrades.contains(UpgradeType.FORTUNE) ? (new FortuneUpgrade().run()) : 1;
            if (i == null) {
                mined.add(new ItemStack(l.getBlock().getType(), 1, (short) l.getBlock().getData()));
            } else {
                i.setAmount(i.getAmount()*multiplier);
                mined.add(i);
            }
            l.getBlock().setType(Material.AIR);
        }
        return true;
    }

    public boolean canBuild(Player p, Block b) {
        WorldGuardPlugin w = RailMiner.getWorldGuard();
        boolean wg = true;
        boolean gp = true;
        boolean kingdoms = false;
        boolean redprotect = true;
        boolean towny = true;
        boolean factions = true;

        //Worldguard
        if (w != null)
            wg = w.canBuild(p, b);

        //GriefPrevention
        if (RailMiner.getPlugin("GriefPrevention")) {
            Claim claim = GriefPrevention.instance.dataStore.getClaimAt(b.getLocation(), true, null);
            if (claim == null) {
                gp = true;
            } else {
                if (claim.allowAccess(p) == null)
                    gp = true;
                else
                    gp = false;
            }
        }

        //Kingdoms
        if (RailMiner.getKingdomManager() == null) {
            kingdoms = true;
        } else {
            LandManager lm = GameManagement.getLandManager();
            SimpleChunkLocation c = new SimpleChunkLocation(b.getLocation().getChunk());
            Land land = lm.getOrLoadLand(c);
            KingdomPlayer kp = Kingdoms.getInstance().getManagers().getPlayerManager().getSession(p);
            Kingdom k = kp.getKingdom();

            if (land.getOwner() == null) {
                kingdoms = true;
            } else {
                if (!kingdoms) {
                    if (k != null) {
                        if (k.getKingdomName().equals(land.getOwner())) {
                            kingdoms = true;
                        } else {
                            kingdoms = false;
                        }
                    } else {
                        kingdoms = false;
                    }
                }
            }

        }

        //RedProtect
        if (RailMiner.getPlugin("RedProtect")) {
            RedProtectAPI redProtectAPI = new RedProtectAPI();
            Region r = redProtectAPI.getRegion(b.getLocation());
            if (r != null)
                redprotect = r.canBuild(p);
        }

        //Towny
        if (RailMiner.getPlugin("Towny")) {
            towny = TownyProtectionTester.test(getOwner(owner), b);
        }

        //Factions
        if (RailMiner.getPlugin("Factions")) {
            MPlayer mp = MPlayer.get(owner);
            Faction f = BoardColl.get().getFactionAt(PS.valueOf(b.getLocation()));
            if (mp.getFaction() == null && f != null)
                factions = false;
            else if (mp.getFaction() != null && f != null) {
                if (!mp.getFaction().getName().equals(f.getName()))
                    factions = false;
            }
        }

        if (!wg)
            return false;
        if (!gp)
            return false;
        if (!kingdoms)
            return false;
        if (!redprotect)
            return false;
        if (!towny)
            return false;
        if (!factions)
            return false;

        return true;
    }

    public void road(Block tip, boolean boost) {
        Location l = tip.getLocation();

        //Start placing stone bricks
        l = up(down(back(back(back(back(back(back(back(l)))))))));
        l.getBlock().setType(Material.STONE_BRICKS);
        l = left(l);
        l.getBlock().setType(Material.STONE_BRICKS);
        l = right(right(l));
        l.getBlock().setType(Material.STONE_BRICKS);

        //Rail placement
        l = up(left(l));
        if (boost) {
            l.getBlock().setType(Material.POWERED_RAIL);
        } else {
            l.getBlock().setType(Material.RAIL);
        }

        //Torches
        if (boost) {
            l = down(left(left(l)));
            l.getBlock().setType(Material.STONE_BRICKS);
            l = up(l);
            l.getBlock().setType(Material.STONE_BRICKS);
            l = right(l);
            l.getBlock().setType(Material.WALL_TORCH,false);
            l.getBlock().setBlockData(getDirection("torch"));
            l = left(up(l));
            l.getBlock().setType(Material.STONE_BRICKS);
            l = right(right(right(right(l))));
            l.getBlock().setType(Material.STONE_BRICKS);
            l = down(l);
            l.getBlock().setType(Material.STONE_BRICKS);
            l = left(l);
            l.getBlock().setType(Material.REDSTONE_WALL_TORCH,false);
            l.getBlock().setBlockData(getDirection("redstonetorch"));
            l = right(down(l));
            l.getBlock().setType(Material.STONE_BRICKS);
        }
    }

    public Player getOwner(UUID uuid) {
        Player p = Bukkit.getServer().getPlayer(uuid);
        if (p == null)
            return null;
        return p;
    }

    //Pass b as the sign
    public boolean checkShape(Block b) {
        boolean debug = RailMiner.getInstance().getConfig().getBoolean("debug");
        if (debug)
            RailMiner.getInstance().getLogger().info("1");
        //Main check
        if (!(b.getType() == Material.WALL_SIGN)) return false;

        if (debug)
            RailMiner.getInstance().getLogger().info("2");
        Block a = null;
        BlockFace face = null;
        for (BlockFace f : BlockFace.values()) {
            org.bukkit.block.data.type.WallSign s = (WallSign) b.getState().getBlockData();
            face = me.bumblebee.railminer.utils.Sign.getAttachedFace(s);
            a = b.getRelative(face);
        }
        if (a == null) return false;
        Location l = a.getLocation();

        if (debug)
            RailMiner.getInstance().getLogger().info("3");

        switch (face) {
            case EAST:
                forward = "x:1";
                back = "x:-1";
                left = "z:-1";
                right = "z:1";
                break;
            case WEST:
                forward = "x:-1";
                back = "x:1";
                left = "z:1";
                right = "z:-1";
                break;
            case NORTH:
                forward = "z:-1";
                back = "z:1";
                left = "x:-1";
                right = "x:1";
                break;
            case SOUTH:
                forward = "z:1";
                back = "z:-1";
                left = "x:1";
                right = "x:-1";
                break;
        }

        //Check updates first
        Block updates = up(forward(forward(forward(forward(b.getLocation().clone()))))).getBlock();
        if (updates.getType() == Material.CHEST) {
            Chest c = (Chest) updates.getState();
            boolean updateUpdates = rawUpgrades.isEmpty();
            if (updateUpdates)
                rawUpgrades = Arrays.asList(c.getInventory().getContents());
            for (ItemStack is : c.getInventory().getContents()) {
                if (is == null)
                    continue;
                if (!is.hasItemMeta())
                    continue;
                if (!is.getItemMeta().hasDisplayName())
                    continue;

                String dis = is.getItemMeta().getDisplayName();
                UpgradeType ut;
                String[] data = ChatColor.stripColor(dis).split(" ");
                if (data.length > 2) {
                    ut = UpgradeType.fromString(data[0] + "_" + data[1]);
                } else {
                    ut = UpgradeType.fromString(data[0]);
                    if (ut == null)
                        continue;
                }
                c.getInventory().removeItem(is);

                if (updateUpdates) {
                    upgrades.add(ut);
                }
            }
        }

        //BOTTOM BACK
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("4");
        l = back(l);
        if (!(l.getBlock().getType() == Material.WALL_SIGN)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("5");
        l = forward(down(l));
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("6");
        l = left(l);
        if (!(l.getBlock().getType() == Material.FURNACE)) {
            //if (!(l.getBlock().getType() == Material.BURNING_FURNACE)) {
                return false;
            //}
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("7");
        l = right(right(l));
        if (!(l.getBlock().getType() == Material.FURNACE)) {
            //if (!(l.getBlock().getType() == Material.BURNING_FURNACE)) {
              return false;
            //}
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("8");
        l = forward(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("9");
        l = left(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("10");
        l = left(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("11");
        l = forward(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("12");
        l = right(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("13");
        l = right(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("14");
        l = forward(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("15");
        l = left(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("16");
        l = left(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("17");
        l = forward(l);
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("18");
        l = right(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("19");
        l = right(l);
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        //BOTTOM POINT
        if (debug)
            RailMiner.getInstance().getLogger().info("20");
        l = left(forward(l));
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        //MIDDLE POINT
        if (debug)
            RailMiner.getInstance().getLogger().info("21");
        l = up(forward(l));
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("22");
        l = back(left(l));
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("23");
        l = right(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("24");
        l = right(l);
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("25");
        l = back(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("26");
        l = left(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("27");
        l = left(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("28");
        l = back(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("29");
        l = right(l);
        if (!(l.getBlock().getType() == Material.REDSTONE_WIRE)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("30");
        l = right(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("31");
        l = back(l);
        if (!(l.getBlock().getType() == Material.REPEATER)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("32");
        l = left(l);
        if (!(l.getBlock().getType() == Material.REDSTONE_WIRE)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("33");
        l = left(l);
        if (!(l.getBlock().getType() == Material.REPEATER)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("34");
        l = back(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("35");
        l = right(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("36");
        l = right(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("37");

        //TODO problem here?
        //MIDDLE BACK RIGHT
        if (upgrades.contains(UpgradeType.STORAGE) || upgrades.contains(UpgradeType.STORAGE_II)) {
            l = back(l);
            if (debug)
                RailMiner.getInstance().getLogger().info("Pre 38a: " + l.getBlock().getType());
            if (!(l.getBlock().getType() == Material.CHEST)) return false;
            if (debug)
                RailMiner.getInstance().getLogger().info("38a");
            l = left(left(l));
            if (debug)
                RailMiner.getInstance().getLogger().info("Pre 39a: " + l.getBlock().getType());
            if (!(l.getBlock().getType() == Material.CHEST)) return false;
            if (debug)
                RailMiner.getInstance().getLogger().info("39a");
        } else {
            l = back(l);
            if (debug)
                RailMiner.getInstance().getLogger().info("Pre 38b: " + l.getBlock().getType());
            if (!(l.getBlock().getType() == Material.DISPENSER)) return false;
            if (debug)
                RailMiner.getInstance().getLogger().info("38b");
            l = left(left(l));
            if (debug)
                RailMiner.getInstance().getLogger().info("Pre 39b: " + l.getBlock().getType());
            if (!(l.getBlock().getType() == Material.DISPENSER)) return false;
            if (debug)
                RailMiner.getInstance().getLogger().info("39b");
        }
        l = right(up(l));
        if (!(l.getBlock().getType() == Material.CHEST)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("40");
        l = right(forward(l));
        if (!(l.getBlock().getType() == Material.REDSTONE_WIRE)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("41");
        l = left(l);
        if (!(l.getBlock().getType() == Material.LEVER)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("42");
        l = left(l);
        if (!(l.getBlock().getType() == Material.REDSTONE_WIRE)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("43");
        l = forward(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("44");
        l = right(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("45");
        l = right(l);
        if (!(l.getBlock().getType() == Material.IRON_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("46");
        l = forward(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("47");
        l = left(l);
        if (!(l.getBlock().getType() == Material.CHEST)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("48");
        l = left(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("49");
        l = forward(l);
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("50");
        l = right(l);
        if (!(l.getBlock().getType() == Material.STICKY_PISTON)) {
            if (!(l.getBlock().getType() == Material.PISTON_HEAD)) {
                return false;
            }
        }
        if (debug)
            RailMiner.getInstance().getLogger().info("51");
        l = right(l);
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        //TOP POINT
        if (debug)
            RailMiner.getInstance().getLogger().info("52");
        l = left(forward(l));
        if (!(l.getBlock().getType() == Material.DIAMOND_BLOCK)) return false;
        if (debug)
            RailMiner.getInstance().getLogger().info("53");
        return true;
    }

    public BlockData getDirection( String ob) {

        if (ob.equalsIgnoreCase("piston")) {
            Piston piston = (Piston) Bukkit.createBlockData(Material.STICKY_PISTON);
            switch (direction) {
                case "WEST":
                    piston.setFacing(BlockFace.WEST);
                case "EAST":
                    piston.setFacing(BlockFace.EAST);
                case "NORTH":
                    piston.setFacing(BlockFace.NORTH);
                case "SOUTH":
                    piston.setFacing(BlockFace.SOUTH);
            }
            return piston;
        } else if (ob.equalsIgnoreCase("diode")) {
            Repeater repeater = (Repeater) Bukkit.createBlockData(Material.REPEATER);
            switch (direction) {
                case "WEST":
                    repeater.setFacing(BlockFace.EAST);
                case "EAST":
                    repeater.setFacing(BlockFace.WEST);
                case "NORTH":
                    repeater.setFacing(BlockFace.SOUTH);
                case "SOUTH":
                    repeater.setFacing(BlockFace.NORTH);
            }
            return repeater;
        } else if (ob.equalsIgnoreCase("backwards-c")) {
            org.bukkit.block.data.type.Chest chest = (org.bukkit.block.data.type.Chest) Bukkit.createBlockData(Material.CHEST);
            switch (direction) {
                case "WEST":
                    chest.setFacing(BlockFace.EAST);
                case "EAST":
                    chest.setFacing(BlockFace.WEST);
                case "NORTH":
                    chest.setFacing(BlockFace.SOUTH);
                case "SOUTH":
                    chest.setFacing(BlockFace.NORTH);
            }
            return chest;
        } else if (ob.equalsIgnoreCase("backwards-d")) {
            Dispenser dispenser = (Dispenser) Bukkit.createBlockData(Material.DISPENSER);
            switch (direction) {
                case "WEST":
                    dispenser.setFacing(BlockFace.EAST);
                case "EAST":
                    dispenser.setFacing(BlockFace.WEST);
                case "NORTH":
                    dispenser.setFacing(BlockFace.SOUTH);
                case "SOUTH":
                    dispenser.setFacing(BlockFace.NORTH);
            }
            return dispenser;
        } else if (ob.equalsIgnoreCase("backwards-f")) {
            org.bukkit.block.data.type.Furnace furnace = (org.bukkit.block.data.type.Furnace) Bukkit.createBlockData(Material.FURNACE);
            switch (direction) {
                case "WEST":
                    furnace.setFacing(BlockFace.EAST);
                case "EAST":
                    furnace.setFacing(BlockFace.WEST);
                case "NORTH":
                    furnace.setFacing(BlockFace.SOUTH);
                case "SOUTH":
                    furnace.setFacing(BlockFace.NORTH);
            }
            return furnace;
        } else if (ob.equalsIgnoreCase("backwards-s")) {
            WallSign wallSign = (WallSign) Bukkit.createBlockData(Material.WALL_SIGN);
            switch (direction) {
                case "WEST":
                    wallSign.setFacing(BlockFace.EAST);
                case "EAST":
                    wallSign.setFacing(BlockFace.WEST);
                case "NORTH":
                    wallSign.setFacing(BlockFace.SOUTH);
                case "SOUTH":
                    wallSign.setFacing(BlockFace.NORTH);
            }
            return wallSign;
        } else if (ob.equalsIgnoreCase("lever")) {
            Switch lever = (Switch) Bukkit.createBlockData(Material.LEVER);
            lever.setFace(Switch.Face.FLOOR);
            return lever;
        } else if (ob.equalsIgnoreCase("torch")) {
            Directional torch = (Directional) Bukkit.createBlockData(Material.WALL_TORCH);
            switch (direction) {
                case "WEST":
                    torch.setFacing(BlockFace.NORTH);
                case "EAST":
                    torch.setFacing(BlockFace.SOUTH);
                case "NORTH":
                    torch.setFacing(BlockFace.EAST);
                case "SOUTH":
                    torch.setFacing(BlockFace.WEST);

            }
            return (BlockData) torch;
        } else if (ob.equalsIgnoreCase("redstonetorch")) {
            RedstoneWallTorch rwTorch = (RedstoneWallTorch) Bukkit.createBlockData(Material.REDSTONE_WALL_TORCH);
            switch (direction) {
                case "WEST":
                    rwTorch.setFacing(BlockFace.SOUTH);
                case "EAST":
                    rwTorch.setFacing(BlockFace.NORTH);
                case "NORTH":
                    rwTorch.setFacing(BlockFace.WEST);
                case "SOUTH":
                    rwTorch.setFacing(BlockFace.EAST);
            }
            return rwTorch;
        }
        return null;
    }

    public boolean checkWater(Block b) {
        if (b.getType() == Material.WATER)
            return true;
        if (b.getType() == Material.BUBBLE_COLUMN){
            return true;
        }
        if (b.getBlockData() instanceof Waterlogged){
            Waterlogged wLog = (Waterlogged) b.getBlockData();
            if (wLog.isWaterlogged()){
                return true;
            }
        }
        return false;
    }

    public boolean checkLava(Block b) {
        if (b.getType() == Material.LAVA)
            return true;
        return false;
    }

    public Location getSignLocation() { return sign.getLocation(); }

    public Location up(Location l) {
        return l.add(0,1,0);
    }
    public Location down(Location l) {
        return l.subtract(0,1,0);
    }
    public Location left(Location l) {
        String[] par = left.split(":");
        int dir = Integer.parseInt(par[1]);
        if (par[0].equalsIgnoreCase("x")) {
            return l.add(dir,0,0);
        } else {
            return l.add(0,0,dir);
        }
    }
    public Location right(Location l) {
        String[] par = right.split(":");
        int dir = Integer.parseInt(par[1]);
        if (par[0].equalsIgnoreCase("x")) {
            return l.add(dir,0,0);
        } else {
            return l.add(0,0,dir);
        }
    }
    public Location forward(Location l) {
        String[] par = forward.split(":");
        int dir = Integer.parseInt(par[1]);
        if (par[0].equalsIgnoreCase("x")) {
            return l.add(dir,0,0);
        } else {
            return l.add(0,0,dir);
        }
    }
    public Location back(Location l) {
        String[] par = back.split(":");
        int dir = Integer.parseInt(par[1]);
        if (par[0].equalsIgnoreCase("x")) {
            return l.add(dir,0,0);
        } else {
            return l.add(0,0,dir);
        }
    }

    public UUID getPlayer() { return owner;}
}
