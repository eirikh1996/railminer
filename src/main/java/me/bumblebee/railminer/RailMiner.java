package me.bumblebee.railminer;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import me.bumblebee.railminer.commands.FriendCommands;
import me.bumblebee.railminer.events.*;
import me.bumblebee.railminer.managers.LimitManager;
import me.bumblebee.railminer.managers.Messages;
import me.bumblebee.railminer.managers.RecipeManager;
import me.bumblebee.railminer.managers.UpgradeManager;
import me.bumblebee.railminer.utils.Friends;
import me.bumblebee.railminer.utils.Protect;
import me.bumblebee.railminer.utils.Stats;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import com.songoda.kingdoms.main.Kingdoms;
import com.songoda.kingdoms.manager.game.GameManagement;

public class RailMiner extends JavaPlugin {

    private static GameManagement kingdomsmanager = null;
    private static Plugin pl;
    Protect p = new Protect();
    Friends f = new Friends();
    LimitManager limits = new LimitManager();
    UpgradeManager upgrades = new UpgradeManager();
    RecipeManager recipes = new RecipeManager();

    public void onEnable() {
        pl = this;
        saveDefaultConfig();
        registerEvents();
        p.loadData();
        f.loadData();
        limits.setup();
        upgrades.getYaml();
        Messages.getYaml();
        recipes.registerRecipes();

        if(Bukkit.getPluginManager().getPlugin("Kingdoms") != null){
            try {
                kingdomsmanager = ((Kingdoms) Bukkit.getPluginManager().getPlugin("Kingdoms")).getManagers();
            }catch (Exception e) {
                //No kingdoms found
            }
        }

        Metrics metrics = new Metrics(this);
        Stats stat = new Stats();
        stat.start();
        protectionChecker();
    }


    public void onDisable() {
        p.saveData();
        f.saveData();
        stopAllMiners();
    }

    public void protectionChecker() {
        if (getWorldGuard() != null)
            Bukkit.getServer().getConsoleSender().sendMessage("[RailMiner] Enabling support for " + ChatColor.GREEN + "WorldGuard");
        if (getPlugin("GriefPrevention"))
            Bukkit.getServer().getConsoleSender().sendMessage("[RailMiner] Enabling support for " + ChatColor.GREEN + "GriefPrevention");
        if (getKingdomManager() != null)
            Bukkit.getServer().getConsoleSender().sendMessage("[RailMiner] Enabling support for " + ChatColor.GREEN + "Kingdoms");
        if (getPlugin("RedProtect"))
            Bukkit.getServer().getConsoleSender().sendMessage("[RailMiner] Enabling support for " + ChatColor.GREEN + "RedProtect");
        if (getPlugin("Towny"))
            Bukkit.getServer().getConsoleSender().sendMessage("[RailMiner] Enabling support for " + ChatColor.GREEN + "Towny");
        if (getPlugin("Factions"))
            Bukkit.getServer().getConsoleSender().sendMessage("[RailMiner] Enabling support for " + ChatColor.GREEN + "Factions " + ChatColor.RED + "- Make sure you are using Factions (MassiveCore) or you will get errors.");
    }

    public static WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null;
        }
        return ((WorldGuardPlugin) plugin);
    }

    public static GameManagement getKingdomManager() {
        return kingdomsmanager;
    }

    public static boolean getPlugin(String name) {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin(name);
        if (plugin == null) {
            return false;
        }

        return true;
    }

    public static Plugin getInstance() {
        return pl;
    }

    public static void stopAllMiners() {
        for (Miner m : Miner.miners.values()) {
            m.stop();
        }
    }

    public void registerEvents() {
        Bukkit.getServer().getPluginCommand("railminer").setExecutor(new FriendCommands());
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerInteract(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerQuit(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new SignChange(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new BlockPlace(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new BlockBreak(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new InventoryClick(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PrepareItemCraft(), this);
    }

}
