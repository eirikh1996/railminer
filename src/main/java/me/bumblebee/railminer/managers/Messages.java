package me.bumblebee.railminer.managers;

import me.bumblebee.railminer.RailMiner;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public enum Messages {

    PREFIX("prefix"),
    INVALID_ARGS("invalidArguments"),
    NO_PERMISSIONS("noPermissions"),
    MINER_IS_PROTECTED("MinerIsProtected"),
    RELOAD_SUCCESS("reloadSuccess"),
    ALREADY_RUNNING("alreadyRunning"),
    INCORRECT_SHAPE("incorrectShape"),
    FOUND_EMPTY_SPACE("foundEmptySpace"),
    OUT_OF_FUEL("outOfFuel"),
    REQUIRES_ITEM("requiresItem"),
    FAILED_TO_MINE("failedToMine"),
    STORAGE_FULL("storageChestsFull"),
    MINER_PROTECTED("minerProtected"),
    FAILED_TO_PROTECT("failedToProtect"),
    BLOCK_LIMIT_REACHED("blockLimitReached"),
    MINER_LIMIT_REACHED("minerLimitReached"),
    UPGRADE_WHILE_RUNNING("upgradeWhileRunning"),
    UPGRADE_LIMIT_REACHED("upgradeLimitReached"),
    INVALID_UPGRADE("invalidUpgrade"),
    NO_FRIENDS("noFriends"),
    ALREADY_FRIENDS("alreadyFriends"),
    NOT_ONLINE("notOnline"),
    FRIEND_ADDED("friendAdded"),
    FRIEND_REMOVED("friendRemoved"),
    NOT_FRIENDS("notFriends"),
    WORLD_DISABLED("worldDisabled"),
    FRIENDS_WITH("friendsWith");

    String key;
    Messages(String key) {
        this.key = key;
    }

    public String get() {
        YamlConfiguration c = getYaml();
        if (c.getString(key) == null)
            return ChatColor.translateAlternateColorCodes('&', "&c[RailMiner] Failed to find a message " + key + "! Please report this to an administrator");
        return ChatColor.translateAlternateColorCodes('&', c.getString("prefix") + " " + c.getString(key));
    }

    public static YamlConfiguration getYaml() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "messages.yml");
        YamlConfiguration c = null;
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            c = YamlConfiguration.loadConfiguration(f);
            c.set("prefix", "&e[&5RailMiner&e]");
            c.set("invalidArguments", "&cInvalid arguments!");
            c.set("noPermissions", "&cYou do not have the required permissions!");
            c.set("minerIsProtected", "&cYou cannot use this miner!");
            c.set("reloadSuccess", "&aSuccessfully reloaded config!");
            c.set("alreadyRunning", "&cThat Miner is already running!");
            c.set("incorrectShape", "&cIncorrect shape!");
            c.set("foundEmptySpace", "&cFound an empty space! Stopping..");
            c.set("outOfFuel", "&cOut of fuel! Stopping..");
            c.set("requiresItem", "&cInsufficient items! Missing %item%");
            c.set("failedToMine", "&cMiner has encountered a problem! Stopping..");
            c.set("storageChestsFull", "&cYour miners chest is full! Stopping..");
            c.set("minerProtected", "&aMiner successfully protected!");
            c.set("failedToProtect", "&cFailed to protect Miner! Is it the right shape?");
            c.set("blockLimitReached", "&cYou have reached your block limit of %limit%. Stopping miner..");
            c.set("minerLimitReached", "&cYou have reached your miner limit of %limit%!");
            c.set("upgradeWhileRunning", "&cYou cannot access upgrades while the miner is running!");
            c.set("upgradeLimitReached", "&cYou can only have %amount% of upgrades!");
            c.set("invalidUpgrade", "&cMiner contains invalid upgrade!");
            c.set("noFriends", "&cNo one else has access to your miners.");
            c.set("alreadyFriends", "&c%name% already has access to your miners!");
            c.set("notOnline", "&c%name% is not online!");
            c.set("friendAdded", "&a%name% can now access your miners!");
            c.set("friendRemoved", "&a%name% no longer has access to your miners!");
            c.set("notFriends", "&c%name% does not have access to your miners!");
            c.set("friendsWith", "&aThe people that have access to your miners are: %list%");
            c.set("worldDisabled", "&cRailMiner is disabled in the world you are in");

            try {
                c.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return c == null ? YamlConfiguration.loadConfiguration(f) : c;
    }

    public static void saveYaml(YamlConfiguration c) {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "messages.yml");
        if (!f.exists())
            getYaml();

        try {
            c.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
