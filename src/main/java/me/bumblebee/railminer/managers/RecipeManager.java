package me.bumblebee.railminer.managers;

import me.bumblebee.railminer.RailMiner;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.Set;

public class RecipeManager {

    UpgradeManager upgrades = new UpgradeManager();

    public void registerRecipes() {
        registerLevelOne();
        registerLevelTwo();
    }

    public void registerLevelOne() {
        YamlConfiguration c = upgrades.getYaml();
        ConfigurationSection recipes = c.getConfigurationSection("recipes");
        if (recipes == null)
            return;

        for (String recipeName : recipes.getKeys(false)) {
            if (c.getConfigurationSection("recipes." + recipeName + ".ingredients") == null)
                continue;

            Set<String> ingredients = c.getConfigurationSection("recipes." + recipeName + ".ingredients").getKeys(false);
            List<String> slots = c.getStringList("recipes." + recipeName + ".recipe");
            String display = ChatColor.translateAlternateColorCodes('&', c.getString("recipes." + recipeName + ".result"));

            if (slots.size() < 3) {
                RailMiner.getInstance().getLogger().warning("Recipe " + recipeName + " does not have 3 rows in recipe shape!");
                continue;
            }
            String lineOne = slots.get(0);
            String lineTwo = slots.get(1);
            String lineThree = slots.get(2);

            ItemStack result = new ItemStack(Material.ENCHANTED_BOOK);
            ItemMeta rm = result.getItemMeta();
            rm.setDisplayName(display);
            result.setItemMeta(rm);

            ShapedRecipe recipe = new ShapedRecipe(result);
            recipe.shape(lineOne, lineTwo, lineThree);
            for (String ingredientID : ingredients) {
                char IID = ingredientID.charAt(0);
                String matStr = c.getString("recipes." + recipeName + ".ingredients." + ingredientID);
                Material mat;
                try {
                    mat = Material.matchMaterial(matStr);
                } catch (Exception e) {
                    RailMiner.getInstance().getLogger().warning("Failed to find ingredient name " + matStr + " for recipe " + recipeName);
                    continue;
                }
                recipe.setIngredient(IID, mat);
            }
            RailMiner.getInstance().getServer().addRecipe(recipe);
        }
    }

    public void registerLevelTwo() {
        ItemStack result = new ItemStack(Material.ENCHANTED_BOOK);
        ItemMeta rm = result.getItemMeta();
        rm.setDisplayName(ChatColor.RED + "Storage II Upgrade");
        result.setItemMeta(rm);

        ShapelessRecipe recipe = new ShapelessRecipe(result);
        recipe.addIngredient(Material.ENCHANTED_BOOK);
        recipe.addIngredient(Material.ENCHANTED_BOOK);
        RailMiner.getInstance().getServer().addRecipe(recipe);
    }

}
