package me.bumblebee.railminer.managers;

import me.bumblebee.railminer.RailMiner;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class UpgradeManager {

    public int getUpgradeLimit() {
        return getYaml().getInt("upgrade-limit");
    }

    public YamlConfiguration getYaml() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "upgrades.yml");
        if (!f.exists())
            RailMiner.getInstance().saveResource("upgrades.yml", false);
        return YamlConfiguration.loadConfiguration(f);
    }

    public boolean checkDisplayName(String display) {
        YamlConfiguration c = getYaml();
        ConfigurationSection recipes = c.getConfigurationSection("recipes");
        if (recipes == null)
            return false;

        for (String recipe : recipes.getKeys(false)) {
            String resultName = ChatColor.translateAlternateColorCodes('&', c.getString("recipes." + recipe + ".result"));
            if (display.equals(resultName))
                return true;
            else if (display.equalsIgnoreCase(ChatColor.RED + "Storage II Upgrade"))
                return true;
        }

        return false;
    }
}
