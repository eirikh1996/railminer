package me.bumblebee.railminer.events;

import me.bumblebee.railminer.managers.Messages;
import me.bumblebee.railminer.utils.Protect;
import me.bumblebee.railminer.utils.ProtectedRegion;
import me.bumblebee.railminer.utils.Util;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.List;

public class BlockBreak implements Listener {

    Util u = new Util();
    Protect p = new Protect();

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();

        for (List<ProtectedRegion> rList : Protect.prot.values()) {
            for (ProtectedRegion r : rList) {
                if (!r.isInside(e.getBlock().getLocation())) {
                    continue;
                }

                if (!r.canUse(p)) {
                    p.sendMessage(Messages.MINER_IS_PROTECTED.get());
                    e.setCancelled(true);
                    return;
                }
                    if (u.checkIfSignBroken(e.getBlock())) {
                        r.remove();
                    }
            }
        }
    }

}