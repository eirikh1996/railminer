package me.bumblebee.railminer.events;

import me.bumblebee.railminer.Miner;
import me.bumblebee.railminer.managers.Messages;
import me.bumblebee.railminer.utils.Protect;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignChange implements Listener {

    Protect protect = new Protect();

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        if (!e.getLine(0).equalsIgnoreCase("[RailMiner]"))
            return;
        if (!e.getLine(2).equalsIgnoreCase("Protect"))
            return;

        Player p = e.getPlayer();
        Miner m = new Miner(e.getBlock(), p.getUniqueId());

        if (!m.checkShape(e.getBlock())) {
            p.sendMessage(Messages.FAILED_TO_PROTECT.get());
            return;
        }
        protect.protectNewRegion(e.getPlayer(), m);
    }

}