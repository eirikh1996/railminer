package me.bumblebee.railminer.events;

import me.bumblebee.railminer.Miner;
import me.bumblebee.railminer.RailMiner;
import me.bumblebee.railminer.managers.Messages;
import me.bumblebee.railminer.managers.UpgradeManager;
import me.bumblebee.railminer.utils.Protect;
import me.bumblebee.railminer.utils.ProtectedRegion;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class PlayerInteract implements Listener {

    UpgradeManager upgrade = new UpgradeManager();

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getClickedBlock() == null) return;
        Block b = e.getClickedBlock();
        Player p = e.getPlayer();

        for (List<ProtectedRegion> rList : Protect.prot.values()) {
            for (ProtectedRegion r : rList) {
                if (!r.isInside(b.getLocation())) {
                    continue;
                }
                if (!r.canUse(p)) {
                    p.sendMessage(Messages.MINER_IS_PROTECTED.get());
                    e.setCancelled(true);
                    return;
                }
            }
        }

        if (b.getType() == Material.CHEST) {
            if (b.getMetadata("upgradeChest") == null)
                return;

            boolean running = false;
            if (b.getMetadata("upgradeChest").size() != 0) {
                running = b.getMetadata("upgradeChest").get(0).asBoolean();
            }
            if (running) {
                e.setCancelled(true);
                p.sendMessage(Messages.UPGRADE_WHILE_RUNNING.get());
                return;
            } else {
                return;
            }
        }

        if (!(b.getType() == Material.WALL_SIGN)) return;
        Sign s = (Sign) b.getState();
        if (!s.getLine(0).equalsIgnoreCase("[RailMiner]")) return;
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if (RailMiner.getInstance().getConfig().getStringList("disabled-worlds").contains(p.getWorld().getName())) {
            e.setCancelled(true);
            p.sendRawMessage(Messages.WORLD_DISABLED.get());
            return;
        }
        if (!p.hasPermission("railminer.use")) {
            p.sendMessage(Messages.NO_PERMISSIONS.get());
            return;
        }
        if (s.getLine(1).equalsIgnoreCase("Start")) {
            if (Miner.miners.containsKey(b)) {
                p.sendMessage(Messages.ALREADY_RUNNING.get());
                return;
            }
            Miner m = new Miner(b, p.getUniqueId());

            if (!m.checkShape(b)) {
                m.upgradeBlock = m.forward(m.forward(m.forward(m.forward(m.up(m.sign.getLocation().clone()))))).getBlock();
                Chest uc = (Chest) m.upgradeBlock.getState();
                if (uc != null) {
                    for (ItemStack is : m.rawUpgrades) {
                        if (is != null && m.placeUpgrades)
                            uc.getBlockInventory().addItem(is);
                    }
                }
                p.sendMessage(Messages.INCORRECT_SHAPE.get());
                return;
            }
            m.start();
        } else if (s.getLine(1).equalsIgnoreCase("Stop")) {
            if (!Miner.miners.containsKey(b)) return;
            Miner m = Miner.miners.get(b);
            m.stop();
        }
    }
}
