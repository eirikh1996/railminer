package me.bumblebee.railminer.events;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.Set;

public class InventoryClick implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getCurrentItem() == null)
            return;
        if (!e.getCurrentItem().hasItemMeta())
            return;
        if (e.getInventory().getName().equalsIgnoreCase("Miner Upgrades")) {
            if (e.getCurrentItem().getType() == Material.WHITE_STAINED_GLASS_PANE) {
                e.setCancelled(true);
                return;
            }
        }
    }

}