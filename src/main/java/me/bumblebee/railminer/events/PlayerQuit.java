package me.bumblebee.railminer.events;

import me.bumblebee.railminer.Miner;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class PlayerQuit implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        List<Block> blocks = new ArrayList<>();
        if (!Miner.playerMiners.containsKey(e.getPlayer().getUniqueId()))
            return;
        for (Block b : Miner.playerMiners.get(e.getPlayer().getUniqueId())) {
            blocks.add(b);
        }
        for (Block b : blocks) {
            if (!Miner.miners.containsKey(b)) continue;
            Miner m = Miner.miners.get(b);
            m.stop();
        }
    }

}
